#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-09-14 15:31:46

#include "Arduino.h"
#include "Arduino.h"
#include "Arduino.h"
#include "SX1278.h"
#include <SPI.h>
#include <stdio.h>

void sx1278_init(uint16_t addr) ;
uint8_t sx1278_packetSend(uint8_t addr, char *msg) ;
uint8_t sx1278_packetReceive(uint16_t timeout) ;
void setup() ;
void loop(void) ;

#include "sx1278_Tx.ino"


#endif
