#include "Arduino.h"
#include "SX1278.h"
#include <SPI.h>

#define LORA_MODE  10
#define LORA_CHANNEL  CH_6_BW_125
#define LORA_Rx_ADDRESS  142

#define LORA_SEND_TO_ADDRESS  141

#define LORA_LED  LED_BUILTIN

#define LORA_RST_Pin	4

int e;
char my_packet[1100];

char message1 [] = "packet reply from lora 2";

void sx1278_init(uint16_t addr)
{
	// Power ON the module
	  if (sx1278.ON() == 0)
	  {
	    Serial.println(F("Setting power ON: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting power ON: ERROR "));
	  }

	  // Set transmission mode and print the result
	  if (sx1278.setMode(LORA_MODE) == 0)
	  {
	    Serial.println(F("Setting Mode: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting Mode: ERROR "));
	  }

	  // Set header
	  if (sx1278.setHeaderON() == 0)
	  {
	    Serial.println(F("Setting Header ON: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting Header ON: ERROR "));
	  }

	  // Select frequency channel
	  if (sx1278.setChannel(LORA_CHANNEL) == 0)
	  {
	    Serial.println(F("Setting Channel: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting Channel: ERROR "));
	  }

	  // Set CRC
	  if (sx1278.setCRC_ON() == 0)
	  {
	    Serial.println(F("Setting CRC ON: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting CRC ON: ERROR "));
	  }

	  // Select output power (Max, High, Intermediate or Low)
	  if (sx1278.setPower('L') == 0)
	  {
	    Serial.println(F("Setting Power: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting Power: ERROR "));
	  }

	  // Set the node address and print the result
	  if (sx1278.setNodeAddress(addr) == 0)
	  {
	    Serial.println(F("Setting node address: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting node address: ERROR "));
	  }

	  // Print a success message
	  Serial.println(F("sx1278 configured finished"));
	  Serial.println();
}

void setup()
{
  pinMode(LORA_LED, OUTPUT);
  digitalWrite(LORA_LED,HIGH);
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  // Print a start message
  Serial.println(F("sx1278 module and Arduino: receiving packets"));

  digitalWrite(LORA_LED,LOW);

  pinMode(LORA_RST_Pin, OUTPUT);
  digitalWrite(LORA_RST_Pin,LOW);
  delay(100);
  digitalWrite(LORA_RST_Pin,HIGH);

  sx1278_init(LORA_Rx_ADDRESS);

  digitalWrite(LORA_LED,HIGH);

}

void loop(void)
{

	//e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS, message1);
  // Receive message for 10 seconds
  e = sx1278.receivePacketTimeout(20000);
  if (e == 0)
  {
    digitalWrite(LORA_LED, LOW);
    delay(50);
    digitalWrite(LORA_LED, HIGH);

    //Serial.println(F("Package received!"));

    for (unsigned int i = 0; i < sx1278.packet_received.length; i++)
    {
      my_packet[i] = (char)sx1278.packet_received.data[i];
    }

    Serial.print(F("Message: "));
    Serial.println(my_packet);

    e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS, message1);
  }
  else
  {
    //Serial.print(F("Package received ERROR\n"));
  }
}
