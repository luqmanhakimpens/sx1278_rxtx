#include "Arduino.h"
#include "Arduino.h"
#include "SX1278.h"
#include <SPI.h>
#include <stdio.h>

#define LORA_MODE  10
#define LORA_CHANNEL  CH_6_BW_125
#define LORA_Tx_ADDRESS  141

#define LORA_SEND_TO_ADDRESS2  142
#define LORA_SEND_TO_ADDRESS3  143

#define LORA_LED  LED_BUILTIN

#define LORA_RST_Pin	2
int e;


char long_message[1100];
char lMessage[] = "0123456789";
String mm="";

char message1 [] = "packet for lora 2";
char message2 [] = "Packet for Lora 3";

char my_packet[100];

void sx1278_init(uint16_t addr)
{
	// Power ON the module
	  if (sx1278.ON() == 0)
	  {
	    Serial.println(F("Setting power ON: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting power ON: ERROR "));
	  }

	  // Set transmission mode and print the result
	  if (sx1278.setMode(LORA_MODE) == 0)
	  {
	    Serial.println(F("Setting Mode: SUCCESS "));
	    sx1278.getMode();
	  }
	  else
	  {
	    Serial.println(F("Setting Mode: ERROR "));
	  }

	  // Set headerra
	  if (sx1278.setHeaderON() == 0)
	  {
	    Serial.println(F("Setting Header ON: SUCCESS "));
	  }
	  else
	  {
	    Serial.println(F("Setting Header ON: ERROR "));
	  }

	  // Select frequency channel
	  if (sx1278.setChannel(LORA_CHANNEL) == 0)
	  {
	    Serial.println(F("Setting Channel: SUCCESS "));
	    sx1278.getChannel();
	  }
	  else
	  {
	    Serial.println(F("Setting Channel: ERROR "));
	  }

	  // Set CRC
	  if (sx1278.setCRC_ON() == 0)
	  {
	    Serial.println(F("Setting CRC ON: SUCCESS "));
	    //sx1278.getChannel();
	  }
	  else
	  {
	    Serial.println(F("Setting CRC ON: ERROR "));
	  }

	  // Select output power (Max, High, Intermediate or Low)
	  if (sx1278.setPower('M') == 0)
	  {
	    Serial.println(F("Setting Power: SUCCESS "));
	    sx1278.getPower();
	  }
	  else
	  {
	    Serial.println(F("Setting Power: ERROR "));
	  }

	  // Set the node address and print the result
	  if (sx1278.setNodeAddress(addr) == 0)
	  {
	    Serial.println(F("Setting node address: SUCCESS "));
	    sx1278.getNodeAddress();
	  }
	  else
	  {
	    Serial.println(F("Setting node address: ERROR "));
	  }

	  // Print a success message
	  Serial.println(F("sx1278 configured finished"));
	  Serial.println();
}

void setup()
{
  pinMode(LORA_LED, OUTPUT);
  digitalWrite(LORA_LED,LOW);

  //Reset sx1278


  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  // Print a start message
  Serial.println(F("sx1278 module and Arduino: send two packets (One to an addrees and another one in broadcast)"));

  digitalWrite(LORA_LED,HIGH);

  pinMode(LORA_LED, OUTPUT);
  digitalWrite(LORA_RST_Pin,LOW);
  delay(100);
  digitalWrite(LORA_RST_Pin,HIGH);

  sx1278_init(LORA_Tx_ADDRESS);

  digitalWrite(LORA_LED,LOW);

}

void loop(void)
{
  // Send message1 and print the result

	mm="";
	for(int f=0; f<=24;++f)
	{
		mm+=lMessage;
		mm.toCharArray(long_message, mm.length());
	}


  e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS2, long_message);


  if (e == 0)
  {
	  Serial.println(F("\nPacket sent to LORA 2:"));
	  //Serial.println(LORA_SEND_TO_ADDRESS2, DEC);
	  sx1278.getRSSI();
      //digitalWrite(LORA_LED, HIGH);
      //delay(500);
      //digitalWrite(LORA_LED, LOW);
	  e=0;
  }
  delay(300);

  e = sx1278.receivePacketTimeout(3000);
  Serial.println(e,DEC);

  if (e == 0)
  {
	  digitalWrite(LORA_LED, HIGH);
	  delay(50);
	  digitalWrite(LORA_LED, LOW);

	  Serial.println(F("Package received!"));

	  for (unsigned int i = 0; i < sx1278.packet_received.length; i++)
	  {
		my_packet[i] = (char)sx1278.packet_received.data[i];
	  }

	  Serial.print(F("Message: "));
	  Serial.println(my_packet);

	  //e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS, message1);
  }
  //delay(100);

  // Send message2 broadcast and print the result

  /*
  e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS3, message2);
  //Serial.print(F("Packet sent, state "));
  //Serial.println(e, DEC);

  if (e == 0)
  {
	  Serial.println(F("Packet sent to LORA 3"));
	  //Serial.println(LORA_SEND_TO_ADDRESS, DEC);
	  sx1278.getRSSI();
      //digitalWrite(LORA_LED, HIGH);
      //delay(50);
      //digitalWrite(LORA_LED, LOW);

      e = sx1278.receivePacketTimeout(2000);

      if (e == 0)
      {
    	  digitalWrite(LORA_LED, LOW);
    	  delay(50);
    	  digitalWrite(LORA_LED, HIGH);

    	  Serial.println(F("Package received!"));

	  for (unsigned int i = 0; i < sx1278.packet_received.length; i++)
	  {
		  my_packet[i] = (char)sx1278.packet_received.data[i];
	  }

	  Serial.print(F("Message: "));
	  Serial.println(my_packet);

	  //e = sx1278.sendPacketTimeout(LORA_SEND_TO_ADDRESS, message1);
	}
  }
	*/

  //delay(2000);
}
